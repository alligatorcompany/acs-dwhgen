import glob
import logging
import pathlib
import sys
from dataclasses import dataclass, field
from typing import Dict, List, Optional

from dataclasses_json import dataclass_json

LOG = logging.getLogger("dv_lineage")


@dataclass_json
@dataclass
class SourceSystem:
    deployable: bool
    hidden_in_system_deployment: bool
    source_type_id: str
    source_type_name: str
    source_type_url: str
    system_color: str
    system_comment: Optional[str]
    system_id: str
    system_name: str


@dataclass_json
@dataclass
class JdbcDataType:
    character_maximum_length: int
    data_type_name: str
    jdbc_data_type_id: int
    numeric_precision: Optional[int]
    numeric_scale: Optional[int]


@dataclass_json
@dataclass
class Column:
    column_id: str
    column_order: int
    column_name: Optional[str] = None
    complete_data_type: Optional[str] = None
    jdbc_data_type: Optional[JdbcDataType] = None
    column_comment: Optional[str] = None


@dataclass_json
@dataclass
class StagingTable:
    system_id: str
    columns: List[Column]
    loading_batch_size: int
    source_schema_name: str
    source_schema_or_system_id: str
    source_table_id: str
    source_table_name: str
    source_table_staging_id: str
    source_table_type_id: str
    staging_table_comment: Optional[str]
    staging_table_display_string: str
    staging_table_id: str
    staging_table_name: str
    where_clause_delta_part_template: Optional[str]
    where_clause_general_part: Optional[str]
    source_system: Optional[SourceSystem] = None


@dataclass_json
@dataclass
class HubLoad:
    business_key_prefix: str
    business_key_short: str
    column_ids: List[Column]
    datavault_category_id: str
    hub_id: str
    hub_load_display_name: str
    hub_load_id: str
    keys_are_prehashed: bool
    keys_are_unique: bool
    staging_table_id: str
    system_id: str
    system_name: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class LinkLoad:
    link_id: str
    link_load_display_name: str
    link_load_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class Satellite:
    column_ids: List[Column]
    functional_suffix_id: str
    functional_suffix_name: str
    hub_id: str
    satellite_comment: str
    satellite_id: str
    satellite_is_prototype: bool
    satellite_subject_area_name: str
    staging_resource_id: str
    staging_table_id: str
    system_id: str
    source_system: Optional[SourceSystem] = None
    stage: Optional[StagingTable] = None


@dataclass_json
@dataclass
class Hub:
    hub_comment: Optional[str]
    hub_id: str
    hub_id_of_alias_parent: Optional[str]
    hub_name: str
    hub_name_of_alias_parent: str
    hub_subject_area_name: str
    satellites: List[Satellite] = field(default_factory=list, init=False)
    loads: List[HubLoad] = field(default_factory=list, init=False)


@dataclass_json
@dataclass
class Link:
    hub_a_id: str
    hub_b_id: str
    link_comment: str
    link_id: str
    link_subject_area_name: str
    link_suffix_id: Optional[str]
    link_suffix_name: str
    link_type: str
    loads: List[LinkLoad] = field(default_factory=list, init=False)
    hub_a: Optional[Hub] = None
    hub_b: Optional[Hub] = None


@dataclass
class DVB:
    sources: Dict[str, SourceSystem] = field(default_factory=dict, init=False)
    stages: Dict[str, StagingTable] = field(default_factory=dict, init=False)
    hubs: Dict[str, Hub] = field(default_factory=dict, init=False)
    satellites: Dict[str, Satellite] = field(default_factory=dict, init=False)
    links: Dict[str, Link] = field(default_factory=dict, init=False)


def parse_dvb_json(
    dvb_meta_path: str,
) -> DVB:
    dv = DVB()
    json_files = glob.glob(dvb_meta_path + "/**/*.json", recursive=True)
    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hubs":
                fs = open(jsonPath, "r")
                hub = Hub.from_json(fs.read())
                dv.hubs[hub.hub_id] = hub
            case "source_systems":
                fs = open(jsonPath, "r")
                source = SourceSystem.from_json(fs.read())
                dv.sources[source.system_id] = source
    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "staging_tables":
                stage = StagingTable.from_json(open(jsonPath, "r").read())
                stage.source_system = dv.sources[stage.system_id]
                dv.stages[stage.staging_table_id] = stage
            case "satellites":
                sat = Satellite.from_json(open(jsonPath, "r").read())
                sat.stage = dv.stages[sat.staging_table_id]
                sat.source_system = dv.sources[sat.system_id]
                dv.hubs[sat.hub_id].satellites.append(sat)
                dv.satellites[sat.satellite_id] = sat
            case "links":
                link = Link.from_json(open(jsonPath, "r").read())
                link.hub_a = dv.hubs[link.hub_a_id]
                link.hub_b = dv.hubs[link.hub_b_id]
                dv.links[link.link_id] = link
    for json in json_files:
        jsonPath = pathlib.PurePath(json)
        type = jsonPath.parent.name
        match type:
            case "hub_loads":
                hubload = HubLoad.from_json(open(jsonPath, "r").read())
                hubload.stage = dv.stages[hubload.staging_table_id]
                hubload.source_system = dv.sources[hubload.system_id]
                hubload.hub = dv.hubs[hubload.hub_id]
                hubload.hub.loads.append(hubload)
            case "link_loads":
                linkload = LinkLoad.from_json(open(jsonPath, "r").read())
                linkload.stage = dv.stages[linkload.staging_table_id]
                linkload.source_system = dv.sources[linkload.system_id]
                linkload.link = dv.links[linkload.link_id]
                linkload.link.loads.append(linkload)
    return dv


@dataclass
class Attribute:
    """
    jedes attribute wird zu einem SQL-Ausdruck in der projektionsliste des zugehörigen select
    a) sql_function as alias -- wenn sql_function und alias gefüllt sind
    b) name as alias -- wenn name and alias gefüllt sind und sql_funciton=None
    c) name - wenn alias=None
    sollte eine sql-function anwendung finden, dann muss auch zwingend ein alias definiert sein
    """

    name: str
    alias: str
    sql_function: str


@dataclass
class BusinessKey:
    """
    jeder bk wird zu einem cte
    CTE_NAME/key_name as (select [distinct/is_unique] (attributes) from rel_name)
    """

    key_name: str
    rel_name: str
    attributes: List[Attribute]
    is_unique: bool


@dataclass
class Context:
    """
    jeder context wird zu eine DBT modell (Satellit), der inkrementell geladen wird
    da wir nur die logische lineage abbilden benötigen wir hier kein scd2
    attributliste distinct slectieren
    +hashkey vom cbc key (BusinessKey)
    +hashdiff aus attributen für vergleich
    +ldts
    +attributes
    """

    context_name: str
    rel_name: str
    key: BusinessKey
    attributes: List[Attribute]


@dataclass
class CBC:
    """
    jede cbc wird zu einem dbt modell mit dem hub namen
    allen bks als cte
    und einem finalen cte aus dem union aller bk-ctes
    select * from bk1.key_name
    union
    select * from bk2.key_name
    ...
    """

    cbc_name: str
    keyset: List[BusinessKey] = field(default_factory=list, init=False)
    context: List[Context] = field(default_factory=list, init=False)


@dataclass
class NBRLoad:
    """
    jeder nbr-load wird zu einer cte
    alle bk müssen sich immer auf eine rel_name beziehen
    CTE_NAME/load_name as (select distinct link-hash,ldts,cbc1_hash(bk1-columns),cbc2_hash(bk2_cols),... from rel_name)

    die bks müssen sich alle auf denselben rel_name, also source/staging beziehen
    """

    load_name: str
    rel_name: str
    cbc_rel: List[BusinessKey]


@dataclass
class NBR:
    """
    jeder nbr wird zu einem DBT modell / link der mit mehreren cte im union beladen wird
    select * from nbrload1.load_name
    union
    select * from nbrload2.load_name
    ...

    jeder nbrload muss die identische bk-list struktur aufweisen (gleiche anzahl von keys)
    """

    nbr_name: str
    nbr_type: str  # n:m, 1:n, n:1
    loads: List[NBRLoad]


def main():
    """
    das staging benötigen wir nicht, da nur die logische abbildung der quellen auf die hub/link/satellit
    benötigt werden.

    - zugriffe auf die rel_name Relationen werden ref()-funktionen auf die hr/stg dbt modelle
    - die entity-views müssen ergänzt werden um ref()-funktionen auf die hub/link/satellit modelle

    """
    dv = parse_dvb_json("/Users/torsten/Project/fifadwh/01_dv/dvb")
    for hub in dv.hubs.values():
        cbc = CBC(hub.hub_id)
        for hubload in hub.loads:
            hubload.column_ids


if __name__ == "__main__":
    sys.exit(main())
