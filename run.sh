#!/bin/bash

execute() {
    TABLE=$1
    SCHEMA=$2
    ID=$3
    HOST=$(echo ${4} | cut -d@ -f2 | cut -d\/ -f3)
    DB=$(echo ${4} | cut -d@ -f2 | cut -d/ -f2- | cut -d\? -f1 | cut -d/ -f3)
    LOCALPATH=$5
    USER=$6
    PWD=$7

    PGPASSWORD=$PWD psql -h $HOST -U $USER -d $DB \
                            -c "\COPY $SCHEMA.$TABLE  \
                            TO STDOUT CSV HEADER quote '\"' DELIMITER ',' " \
                            |   PIPE_DB=$DB \
                                PIPE_SCHEMA=$SCHEMA \
                                PIPE_TABLE=$TABLE \
                                PIPE_TARGET_LOCAL=$LOCALPATH \
                                PIPE_HOST=$HOST \
                                python ./dbt_dwhgen/csv2parquet.py
}

export -f execute

#git clone --depth=1 --branch $REVISION $REPOURL ~/git/

CORES=$(cat /proc/cpuinfo | awk '/^processor/{print $3}' |wc -l)
cat tables.csv | parallel --bar -j$CORES -C --header --colsep , --skip-first-line execute {1} {2} {3} {4} $PIPE_TARGET_LOCAL $PIPE_USER $PIPE_PWD

AWS_DEFAULT_REGION=$PIPE_REGION AWS_ACCESS_KEY_ID=$PIPE_ACCESS_KEY AWS_SECRET_ACCESS_KEY=$PIPE_SECRET_KEY \
    s5cmd cp $PIPE_TARGET_LOCAL/target/* s3://$PIPE_BUCKET/
